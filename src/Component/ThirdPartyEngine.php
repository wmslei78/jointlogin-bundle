<?php

namespace Wmslei78\Bundle\JointLoginBundle\Component;

class ThirdPartyEngine
{
    const PLATFORM_WEIXIN	=	'weixin';
    const PLATFORM_WEIBO	=	'weibo';
    const PLATFROMS		=	[ self::PLATFORM_WEIXIN, self::PLATFORM_WEIBO ];

    private $guzzle;

    private $weixin_appid;
    private $weixin_secret;

    public function __construct( \GuzzleHttp\Client $guzzle, $weixin_appid, $weixin_secret)
    {
	$this->guzzle   	= $guzzle;
	$this->weixin_appid 	= $weixin_appid;
	$this->weixin_secret 	= $weixin_secret;
    }

    /**
     * @param string $platform
     * @param string $token
     *
     * @return array|null
     */
    public function generateUser($platform, $token) {
        try {
	    if (!(in_array($platform, self::PLATFROMS) && $token)) throw new \Exception();

	    switch ($platform) {
		case self::PLATFORM_WEIXIN:
		    return $this->generateUserFromWeixin($token);
		case self::PLATFORM_WEIBO:
		    return $this->generateUserFromWeibo($token);
		default:
		    throw new \Exception();
	    }
	}
	catch (\Exception $e) {
            return null;
	}
    }

    /**
     * @param string $token
     *
     * @return array
     */
    private function generateUserFromWeixin($token) {
        $response = $this->guzzle->get('https://api.weixin.qq.com/sns/oauth2/access_token', [
	    'query' => [
	        'appid' 	=>	$this->weixin_appid,
		'secret'	=>	$this->weixin_secret,
		'code'		=>	$token,
		'grant_type'	=>	'authorization_code'
	    ]
	]);
	$doc = json_decode($response->getBody(), true);
	$userId = $doc['openid'];
	if (!$userId) throw new \Exception();

	$response = $this->guzzle->get('https://api.weixin.qq.com/sns/userinfo', [
	    'query' => [
	        'access_token' 	=> 	$doc['access_token'],
		'openid' 	=> 	$userId
	    ]
	]);
	$doc = json_decode($response->getBody(), true);

	return [
	    'platform'		=>	self::PLATFORM_WEIXIN,
	    'uid'		=>	$userId,
	    'nickname'		=>	$doc['nickname'],
	    'portrait'		=>	$doc['headimgurl']
	];
    }

    /**
     * @param string $token
     *
     * @return array
     */
    private function generateUserFromWeibo($token) {
	$response = $this->guzzle->get('https://api.weibo.com/2/account/get_uid.json', [
	    'query' => ['access_token' => $token]
	]);
	$doc = json_decode($response->getBody(), true);
	$userId = $doc['uid'];
	if (!$userId) throw new \Exception();

	$response = $this->guzzle->get('https://api.weibo.com/2/users/show.json', [
	    'query' => [
	        'access_token' 	=> 	$token,
		'uid' 		=> 	$userId
	    ]
	]);
	$doc = json_decode($response->getBody(), true);

	return [
	    'platform'		=>	self::PLATFORM_WEIBO,
	    'uid'		=>	$userId,
	    'nickname'		=>	$doc['name'],
	    'portrait'		=>	$doc['avatar_hd'] ?: $doc['avatar_large'] ?: $doc['profile_image_url']
	];
    }
}
