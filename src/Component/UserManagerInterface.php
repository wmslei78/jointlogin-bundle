<?php

namespace Wmslei78\Bundle\JointLoginBundle\Component;

interface UserManagerInterface
{
    /**
     * find user by username.
     *
     * @param string $platform
     * @param string $uid
     * @param string $nickname
     * @param string $portrait
     *
     * @return string
     *
     */
    public function save($platform, $uid, $nickname, $portrait);

    /**
     * find user by username.
     *
     * @param string $username
     *
     * @return object|null
     */
    public function findByUsername($username);
}
