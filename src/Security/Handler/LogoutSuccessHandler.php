<?php

namespace Wmslei78\Bundle\JointLoginBundle\Security\Handler;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

    public function onLogoutSuccess( Request $request )
    {
        return new JsonResponse();
    }

}
