<?php

namespace Wmslei78\Bundle\JointLoginBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class JointUserProvider implements UserProviderInterface
{
    private $engine;

    private $m;

    private $userClass;

    public function __construct( \Wmslei78\Bundle\JointLoginBundle\Component\ThirdPartyEngine $engine,
				 \Wmslei78\Bundle\JointLoginBundle\Component\UserManagerInterface $m,
				 $userClass )
    {
        $this->engine = $engine;
        $this->m = $m;
        $this->userClass = $userClass;
    }

    public function getUsernameForCredentials( $credentials )
    {
        $user = $this->engine->generateUser($credentials['platform'], $credentials['token']);
        return $user ?
	    $this->m->save(
	        $user['platform'],
		$user['uid'],
		$user['nickname'],
		$user['portrait']
	    ) :
	    null;
    }

    public function loadUserByUsername( $username )
    {
        return $this->m->findByUsername( $username );
    }

    public function refreshUser( UserInterface $user )
    {
        return $this->m->findByUsername( $user->getUsername() );
    }

    public function supportsClass( $class )
    {
        return $class === $this->userClass;
    }
}
