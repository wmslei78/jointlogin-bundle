<?php

namespace Wmslei78\Bundle\JointLoginBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class OAuth2TokenAuthenticator extends AbstractGuardAuthenticator
{
    private $targetUrl;

    private $platforms;

    private $rememberMeServices;


    public function __construct( $targetUrl, $platforms )
    {
        $this->targetUrl = $targetUrl;
        $this->platforms = $platforms;
    }

    public function getCredentials( Request $request )
    {
        $url = explode('/', rawurldecode($request->getPathInfo()));
        $platform = array_pop($url);
	$url = implode('/', $url);

        if ($url === $this->targetUrl && $request->getRealMethod() === 'POST')
	    return [
	        'platform'	=>	$platform,
	        'token'		=>	in_array($platform, $this->platforms) ? $request->request->get('token') : null
	    ];
    }

    public function getUser( $credentials, UserProviderInterface $userProvider )
    {
        $username = $userProvider->getUsernameForCredentials( $credentials );
        if ( $username )
            return $userProvider->loadUserByUsername( $username );
    }

    public function checkCredentials( $credentials, UserInterface $user )
    {
        return true;
    }

    public function onAuthenticationFailure( Request $request, AuthenticationException $exception )
    {
        return new JsonResponse(
            [ 'code'    => JsonResponse::HTTP_FORBIDDEN,
              'message' => $exception->getMessage(),
            ],
            JsonResponse::HTTP_FORBIDDEN
        );
    }

    public function onAuthenticationSuccess( Request $request, TokenInterface $token, $providerKey )
    {
        return new JsonResponse();
    }

    public function start( Request $request, AuthenticationException $authException = null )
    {
        return new JsonResponse(
            [ 'code'    => JsonResponse::HTTP_UNAUTHORIZED,
              'message' => $authException ? $authException->getMessage() : 'Oops.',
            ],
            JsonResponse::HTTP_UNAUTHORIZED
        );
    }

    public function supportsRememberMe()
    {
        return true;
    }
}
