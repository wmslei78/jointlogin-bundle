<?php

namespace Wmslei78\Bundle\JointLoginBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class Wmslei78JointLoginExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

	$container->setParameter( 'wmslei78_joint_login.url_prefix', $config['url_prefix'] );
	$container->setParameter( 'wmslei78_joint_login.platforms', $config['platforms'] );
	$container->setParameter( 'wmslei78_joint_login.weixin_appid', $config['weixin_appid'] );
	$container->setParameter( 'wmslei78_joint_login.weixin_secret', $config['weixin_secret'] );
    }
}
