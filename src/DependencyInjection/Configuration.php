<?php

namespace Wmslei78\Bundle\JointLoginBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('wmslei78_joint_login');

	$rootNode
	    ->children()
	    	->scalarNode('url_prefix')->defaultValue('')->end()
	    	->arrayNode('platforms')
	    	    ->defaultValue(['weixin', 'weibo'])
	    	    ->prototype('scalar')->end()
	    	->end()
	    	->scalarNode('weixin_appid')->defaultNull()->end()
	    	->scalarNode('weixin_secret')->defaultNull()->end()
	    ->end()
	;

        return $treeBuilder;
    }
}
