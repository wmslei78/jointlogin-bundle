<?php

namespace Wmslei78\Bundle\JointLoginBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityController
{
    /**
     * @Route("/login")
     * @Method("GET")
     */
    public function accessDeniedAction()
    {
	return new JsonResponse(
	    ['code' => JsonResponse::HTTP_FORBIDDEN,
		'message' => 'Forbidden.',
	    ],
	    JsonResponse::HTTP_FORBIDDEN
	);
    }

    /**
     * @Route("/login/{platform}", requirements={"platform"="weixin|weibo"})
     * @Method("POST")
     */
    public function loginAction()
    {
    }

    /**
     * @Route("/logout")
     */
    public function logoutAction()
    {
    }
}
